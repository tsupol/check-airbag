<?php
$asset_path = "./";
?>
<!DOCTYPE html>
<html>
<head>
  <!-- Note - Do not add Google Analytics code here, will cause conflicts! -->
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NHRPMJ8');</script>
  <!-- End Google Tag Manager -->

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta name="description" content="รถยนต์ของท่านอยู่ในข่ายต้องเปลี่ยนชิ้นส่วนในชุดถุงลมหรือไม่?">
  <title>Check Airbag - รถยนต์ของท่านอยู่ในข่ายต้องเปลี่ยนชิ้นส่วนในชุดถุงลมหรือไม่?</title>
  <link rel="canonical" href="http://check-aribag.artplore.com"/>
  <link rel="stylesheet" type="text/css" media="all" href="<?php echo $asset_path ?>css/main.css?<?php echo time(); ?>"/>
  <link href="https://fonts.googleapis.com/css?family=Kanit:300,400,500,600,700" rel="stylesheet"/>
  <link rel="stylesheet" href="fonts/db_helvethaicax/stylesheet.css" type="text/css" media="all"/>

</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NHRPMJ8"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="app">
  <div id="main-nav">
    <div class="nav-inner">
      <div id="nav-toggle"><span></span></div>
      <a href="#" class="nav-logo">
        <img src="./imgs/logo-main.png"/>
      </a>
      <div class="nav-list">
      </div>
    </div>
  </div>

  <!-- Page Content -->
  <div id="sec-main" class="layout-outer">
    <div class="_bg flex-h-center">
      <!-- For scaling everything proportionally -->
      <div class="scale-wrap">

        <!-- Content: Title & Car -->
        <div class="_content flex-col-center">

          <!-- Title -->
          <div id="title-scale" class="__hidden">
            <div class="_title">
              <div class="_img">
                <img class="" src="./imgs/icon-title.png"/>
              </div>
              <div class="_text">
                <h2 class="_main-header ">
                  รถยนต์ของท่านอยู่ในข่าย
                </h2>
                <h3 class="_sub-header ">
                  ต้อง <span class="_change-span">เปลี่ยนชิ้นส่วนในชุดถุงลม</span> หรือไม่?
                </h3>
              </div>
            </div>
          </div>

          <!-- Car -->
          <div id="car" class="_car">
            <img src="./imgs/car.png"/>
            <!-- Below the Car -->
            <div class="_below-car flex-h-center">
              <div>
                <img src="./imgs/srs.png"/>
              </div>
            </div>
          </div>

        </div>

        <!-- Brand Select Footer -->
        <div id="brand-select" class="layout-outer">
          <div class="bs-content flex-col-center">
            <h2 class="text-center">
              คลิกเลือกแบรนด์รถยนต์ด้านล่าง<br class="show-sm"/>
              เพื่อตรวจสอบ
            </h2>
            <div class="link-wrap">
              <a href="https://lead.bmw.co.th/technical-campaign/th/" class="brand-link" target="_blank">
                <div class="_border"></div>
                <div class="_wrap">
                  <img class="__bnw" src="./imgs/logo-bmw.png"/>
                  <span class="_year">ปี 2000-2005</span>
                </div>
              </a>
              <a href="https://vinsearch.honda.co.th/" class="brand-link" target="_blank">
                <div class="_border"></div>
                <div class="_wrap">
                  <img class="__honda" src="./imgs/logo-honda.png"/>
                  <span class="_year">ปี 2001-2014</span>
                </div>
              </a>
              <a href="http://mservice.mazda.co.th/vincheck" class="brand-link" target="_blank">
                <div class="_border"></div>
                <div class="_wrap">
                  <img class="__mazda" src="./imgs/logo-mazda.png"/>
                  <span class="_year">ปี 2004-2014</span>
                </div>
              </a>
              <a href="https://vinsearch.mitsubishi-motors.co.th/" class="brand-link" target="_blank">
                <div class="_border"></div>
                <div class="_wrap">
                  <img class="__mitsubishi" src="./imgs/logo-mitsubishi.png"/>
                  <span class="_year">ปี 2005-2015</span>
                </div>
              </a>
              <a href="https://www.nissan.co.th/owners/recall-nissan.html" class="brand-link" target="_blank">
                <div class="_border"></div>
                <div class="_wrap">
                  <img class="__nissan" src="./imgs/logo-nissan.png"/>
                  <span class="_year">ปี 2000-2014</span>
                </div>
              </a>
              <a href="https://www.toyota.co.th/ssc/toyota/" class="brand-link" target="_blank">
                <div class="_border"></div>
                <div class="_wrap">
                  <img class="__toyota" src="./imgs/logo-toyota.png"/>
                  <span class="_year">ปี 2001-2014</span>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="portrait-mode">
  กรุณาดูในแนวตั้ง
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.6.15/browser-polyfill.min.js"></script>
<script src="./js/index.js?<?php echo time(); ?>"></script>
<script type="text/javascript">
  function gaTrack (eventLabel) {
    ga('send', 'event', {
      eventCategory: 'External Link',
      eventAction: 'click',
      eventLabel: eventLabel
    });
  }
</script>

</body>
</html>


