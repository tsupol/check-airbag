$(document).ready(function () {

  var padding = 30;
  var baseWidth = 850;
  var baseHeight = 810;
  var title = {
    width: $('#title-scale').width(),
    height: $('#title-scale').height(),
  };
  var scale = 1;

  var $footer = $('#brand-select');
  var $car = $('#car');
  var $title = $('#title-scale');


  doScale();
  setTimeout(doScale(), 50);
  setTimeout(function () {
    $('#title-scale').removeClass('__hidden')
  }, 100);

  $(window).scrollTop(0);
  $(window).resize(doScale);
  $(window).on("orientationchange", doScale);

  function removeScaleCSSPortrait () {
    $('._title').removeAttr('style');
    $('#title-scale').removeAttr('style');
  }

  function removeScaleCSSLandscape () {
    $('._title').removeAttr('style');
    $('.scale-wrap').removeAttr('style');
  }

  if (checkIE()) {
    $('._border').css({ opacity: 0 })
  }

  function doScale () {
    var pageHeight = $('._bg').height();
    var winHeight = $(window).height();
    var pageWidth = $(window).width();

    // Fix safari
    // ----------------------------------------
    if(pageWidth <= 767 && pageHeight > winHeight) {
      $('._bg').css({'height': (winHeight - 35) + 'px'});
    }

    // landscape
    if (pageHeight < pageWidth) {
      if (pageHeight < baseHeight) {
        removeScaleCSSPortrait();
        $footer.removeAttr('style');
        scale = (pageHeight / baseHeight);
        console.log('pageHeight', pageHeight);
        console.log('---', $(window).height());
        $('.scale-wrap').css({
          'transform': 'scale(' + scale + ')',
          'height': baseHeight + 'px',
          'top': (((baseHeight * scale) - baseHeight) / 2) + 'px',
        });

        $('._title').css({
          'paddingTop': '20px'
        });

        // IE fix - add margin as transform-scale will push it up.
        // ----------------------------------------
        var titleTop = $title.position().top;
        if (titleTop < -1) {
          $title.css({
            'margin-top': (Math.abs(titleTop) * (1 / scale)) + 'px'
          })
        }

      } else {
        removeScaleCSSLandscape();
      }
    }
    // portrait
    else {
      removeScaleCSSLandscape();
      if (pageWidth < title.width) {
        scale = (pageWidth / title.width);
        $('#title-scale').css({
          'transform': 'scale(' + scale + ')',
          'margin-top': (((title.height * scale) - title.height) / 2) + 'px',
        });
        $('._title').css({
          'paddingTop': '20px'
        });

        // Fix - not too far from the car
        // ----------------------------------------
        if (pageWidth < 460 && winHeight > 644) {
          $footer.css({
            top: ($car.offset().top + $car.height() - 20) + 'px',
            display: 'block',
          })
        } else {
          $footer.removeAttr('style')
        }

      } else {
        removeScaleCSSPortrait()
      }
    }
  }
});

function checkIE () {
  if (navigator.appName == 'Microsoft Internet Explorer' || !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof $.browser !== "undefined" && $.browser.msie == 1)) {
    return true
  }
  return false
}
